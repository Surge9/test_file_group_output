require 'date'

# Perform loading the file list from folder
# Only file names with given format are loaded
# Format is '<id>_<type>_<date>.txt'
# Initialized with path
class FileGroupList

  attr_accessor :filename_groups
  
  # Load filenames on initialize
  # @param options - options hash
  # option :path path to files
  def initialize(options={})
    @path = options[:path] || "."
    @filename_groups = parse_filenames
  end

  # Get correct filenames from the directory using regexp
  # Create internal representation of filenames using hash
  def parse_filenames
    filename_groups = {}
    regexp = /^(\w+)_(account|activity|position|security)_([0-9]{3}(?:[0-9]{2})(?:3[0-1]|[0-2][1-9]|[1-9])(?:[0-1][1-2]|[0-9])).txt/i
    files = Dir["#{@path}/*.*"]
    files.each do |file_name|
      next if File.directory?(file_name)

      # check filename with regexp
      dimensions = regexp.match(File.basename(file_name))
      next if dimensions.nil?

      # dimensions contains id, type and date as match info
      id   = dimensions.captures[0]
      type = dimensions.captures[1]
      date = parse_into_date(dimensions.captures[2])
      next unless date

      # create hash entry
      ((filename_groups[id] ||= {})[date] ||= []) << type
    end

    return filename_groups
  end

  # Return list of filenames loaded into internal hash
  def filenames
    result = []
    @filename_groups.each do |id|
      id[1].each do |date|
        date[1].each do |type|
          result << "#{id.first}_#{type}_#{parse_from_date(date.first)}.txt"
        end
      end
    end
    return result
  end

  private

    # Parse date part of the filename into date
    # 'rescue nil' is used to catch invalid date
    # e.g. string '20151123' to date 2015-11-23
    def parse_into_date(s)
      begin Date.strptime(s, "%Y%m%d") rescue nil end
    end

    # Create string from saved date in a format
    # used in filenames' date part
    # e.g. date 2015-07-14 to string '20150714')
    def parse_from_date(date)
      date.strftime("%Y%m%d")
    end

    def parse_date(s)
      begin Date.strptime(s, "%Y%m%d").strftime("%Y-%m-%d") rescue nil end
    end

end
