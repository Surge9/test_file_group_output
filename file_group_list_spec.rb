# workaround to make FakeFS work without exceptions
# https://github.com/fakefs/fakefs/issues/215#issuecomment-166630141
require 'pp'

require 'fakefs/spec_helpers'
require './file_group_list.rb'

describe FileGroupList do
  include FakeFS::SpecHelpers

  before(:all) do
    @path = "."
    @id_entries_num = 6
    @date_entries_num = 8
    @type_entries_num = 4
  end

  context "with empty initialization" do
    before(:each) do
      FakeFS.activate!
      FakeFS::FileSystem.clone(@path)
    end

    after(:each) do
      FakeFS.deactivate!
    end

    it "should have empty internal hash when given path has no data" do
      fg = FileGroupList.new({path: "."})
      expect(fg.filename_groups).to be_empty
    end

    it "should have empty internal hash when no path is given and default path has no data" do
      fg = FileGroupList.new
      expect(fg.filename_groups).to be_empty
    end
  end
  
  context "with correct input data" do
    before(:each) do
      FakeFS.activate!
      FakeFS::FileSystem.clone(@path)
      generate_correct_files
    end

    after(:each) do
      FakeFS.deactivate!
    end

    it "all files should be stored" do
      fg = FileGroupList.new({path: @path})
      
      files = Dir.entries(@path)
      stored_filenames = fg.filenames

      files.each do |file|
        next if File.directory?(file)
        expect(stored_filenames).to include(file)
      end
    end

  end

  context "with incorrect input data" do
    before(:each) do
      FakeFS.activate!
      FakeFS::FileSystem.clone(@path)
      generate_incorrect_files
    end   
    
    after(:each) do
      FakeFS.deactivate!
    end
    
    it "filename groups should be empty" do
      fg = FileGroupList.new({path: @path})
      expect(fg.filename_groups).to be_empty
    end
  end

  context "with mixed input data" do
    before(:each) do
      FakeFS.activate!
      FakeFS::FileSystem.clone(@path)
      @correct_files   = generate_correct_files
      @incorrect_files = generate_incorrect_files
    end

    after(:each) do
      FakeFS.deactivate!
    end
    
    it "only correct files should be stored" do
      stored_filenames = FileGroupList.new.filenames

      @correct_files.each do |file|
        next if File.directory?(file)
        expect(stored_filenames).to include(file)
      end

      @incorrect_files.each do |file|
        next if File.directory?(file)
        expect(stored_filenames).not_to include(file)
      end
    end

  end

  def generate_correct_files
    result = []
    @id_entries_num.times do
      id = generate_id
      @date_entries_num.times do |i|
        date = generate_date(Date.today + i-1)
        ['Account', 'Activity', 'Position', 'Security'].shuffle.each do |type|
          filename = generate_file_name(id, type, date)
          File.open(filename, "w") {|f|
            f.write ""
            result << filename
          }
        end
      end
    end
    return result
  end

  def generate_incorrect_files
    result = []
    @id_entries_num.times do
      id = generate_id
      @date_entries_num.times do |i|
        date = generate_bad_date
        @type_entries_num.times do |type|
          filename = generate_file_name(id, generate_bad_type, date)
          File.open(filename, "w") {|f| 
            f.write ""
            result << filename
          }
        end
      end
    end
    return result
  end


  def generate_id
    "U#{(0..9).to_a.shuffle[0..4].join}"
  end
  
  def generate_type
    ['Account', 'Activity', 'Position', 'Security'].shuffle[0]
  end
  
  def generate_date(date =Date.today)
    date.strftime("%Y%m%d")
  end
  
  def generate_file_name(id, type, date)
    id_part = id || generate_id
    type_part = type || generate_type
    date_part = date || generate_date
    return "#{id_part}_#{type_part}_#{date_part}.txt"
  end

  def generate_bad_type
    ('a'..'z').to_a.shuffle[0..8].join
  end
  
  def generate_bad_date
    ('0'..'9').to_a.shuffle[0..8].join
  end

end
