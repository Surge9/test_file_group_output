#! /usr/bin/ruby

require './file_group_list.rb'

fg = FileGroupList.new(path: ARGV[0])
fg.filename_groups.each do |id|
  puts "#{id.first}"
  id[1].sort.each do |date|
    puts "    #{date.first.strftime("%Y-%m-%d")}"
    date[1].sort.each do |type|
      puts "        #{type.downcase}"
    end
  end
end
