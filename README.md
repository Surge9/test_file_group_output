# PARSE FILE NAMES #

### Parsing ###

Filenames with a special names are parsed into the structure that could be obtained and be displayed

### Run the parse ###

The script `./run_file_group_list` is started to parse the files. Path to files could be added as a parameter

```
$ ./run_file_group_list ./test_files
U79014
    2015-01-12
        account
        activity
        position
        security
    2015-01-13
        account
        activity
        position
        security
U6342
    2015-01-12
        account
        activity
        position
        security
    2015-01-13
        account
        activity
        position
        security
    2015-01-16
        account
        activity
```

### Testing ###

The RSpec script `file_group_list_spec.rb` is added for testing

```
$ rspec file_group_list_spec.rb --format doc

FileGroupList
  with empty initialization
    should have empty internal hash when given path has no data
    should have empty internal hash when no path is given and default path has no data
  with correct input data
    all files should be stored
  with incorrect input data
    filename groups should be empty
  with mixed input data
    only correct files should be stored

Finished in 2.43 seconds (files took 0.14079 seconds to load)
5 examples, 0 failures
```

### Platform ###

ruby 2.3.0p0 (2015-12-25 revision 53290) [x86_64-linux]

### Additional gems ###

RSpec
```$ gem install rspec```

FakeFS
```$ gem install fakefs```